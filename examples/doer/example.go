package doerExample

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/jakubdal/doer"
)

// RequestExample requests http://example.com/ and returns it's body
func RequestExample(doer doer.HTTPDoer) (string, error) {
	req, err := http.NewRequest(http.MethodGet, "http://example.com/", nil)
	if err != nil {
		return "", errors.Wrap(err, "http.NewRequest")
	}
	resp, err := doer.Do(req)
	if err != nil {
		return "", errors.Wrap(err, "doer.Do")
	}
	defer func() {
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}()

	if resp.StatusCode/100 != 2 {
		return "", fmt.Errorf("invalid status code: %v", resp.StatusCode)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.Wrap(err, "resp.Body")
	}

	return string(b), nil
}
