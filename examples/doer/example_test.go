package doerExample

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"gitlab.com/jakubdal/doer"
)

type readFunc func(p []byte) (n int, err error)

func (rf readFunc) Read(p []byte) (n int, err error) {
	return rf(p)
}

func TestDoer(t *testing.T) {
	tests := []struct {
		name   string
		doFunc func(req *http.Request) (*http.Response, error)

		expectError bool
		out         string
	}{
		{
			name: "doer error",
			doFunc: func(req *http.Request) (*http.Response, error) {
				return nil, fmt.Errorf("expected error")
			},
			expectError: true,
		},
		{
			name: "invalid status code",
			doFunc: func(req *http.Request) (*http.Response, error) {
				return &http.Response{
					StatusCode: http.StatusInternalServerError,
					Body:       ioutil.NopCloser(strings.NewReader("")),
				}, nil
			},
			expectError: true,
		},
		{
			name: "body's read error",
			doFunc: func(req *http.Request) (*http.Response, error) {
				return &http.Response{
					StatusCode: http.StatusOK,
					Body: ioutil.NopCloser(readFunc(func(p []byte) (n int, err error) {
						return 0, fmt.Errorf("expected read error")
					})),
				}, nil
			},
			expectError: true,
		},
		{
			name: "correct response",
			doFunc: func(req *http.Request) (*http.Response, error) {
				return &http.Response{
					StatusCode: http.StatusOK,
					Body:       ioutil.NopCloser(strings.NewReader("expected content")),
				}, nil
			},
			out: "expected content",
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			out, err := RequestExample(doer.MockHTTPDoer{
				DoFunc: tc.doFunc,
			})
			if tc.expectError {
				if err == nil {
					t.Fatalf("expected error not returned")
				}
				return
			}
			if err != nil {
				t.Fatalf("unexpected error returned: %v", err)
			}

			if out != tc.out {
				t.Errorf("invalid out\nexpected:\n\t%+#v\ngot:\n\t%+#v", tc.out, out)
			}
		})
	}
}
