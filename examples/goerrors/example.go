package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/jakubdal/goerror"
)

// CHECK_START OMIT
func checkToken(expectedToken string) func(token string) error {
	return func(token string) error {
		switch token {
		case expectedToken:
			return nil
		case "":
			return goerror.WithHTTPStatusCode(
				goerror.WithUserMessage(
					fmt.Errorf("empty token provided"),
					"EMPTY_TOKEN",
				),
				http.StatusBadRequest,
			)
		default:
			return goerror.WithHTTPStatusCode(
				goerror.WithUserMessage(
					fmt.Errorf("invalid token, expected %v got %v", expectedToken, token),
					"INVALID_TOKEN",
				),
				http.StatusForbidden,
			)
		}
	}
}

// CHECK_END OMIT

// RUN_START OMIT
func main() {
	check := checkToken("expectedToken")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if err := check(r.URL.Query().Get("token")); err != nil {
			fmt.Println("local error:", err)
			http.Error(w, goerror.UserMessage(err), goerror.HTTPStatusCode(err))
			return
		}
		w.WriteHeader(http.StatusNoContent)
	})
	go func() {
		http.ListenAndServe("localhost:8080", nil)
	}()

	for _, token := range []string{
		"", "unexpectedToken", "expectedToken",
	} {
		resp, _ := http.Get("http://localhost:8080?token=" + token)
		b, _ := ioutil.ReadAll(resp.Body)
		fmt.Printf("response: %v\nstatusCode:%v\n\n", strings.TrimSpace(string(b)), resp.StatusCode)
	}
}

// RUN_END OMIT
