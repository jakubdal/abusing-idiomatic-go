package main

import (
	"errors"
	"fmt"
)

// DEF_START OMIT

func NewMyError(devErr, userErr string) MyError {
	return MyError{devErr: devErr, userErr: userErr}
}

type MyError struct {
	devErr  string
	userErr string
}

func (me MyError) Error() string {
	return me.devErr
}

// DEF_END OMIT

// RUN_START OMIT

func printError(err error) {
	switch err := err.(type) {
	case MyError:
		fmt.Printf("developers error: %v\nuser error:%v\n\n", err.devErr, err.userErr)
	default:
		fmt.Println(err)
	}
}

func main() {
	var err error = NewMyError("devErr", "userErr")
	printError(err)
	printError(errors.New("error"))
}

// RUN_END OMIT
