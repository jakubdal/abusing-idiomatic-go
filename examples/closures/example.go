package main

import "fmt"

// NextInts returns a function, that returns consecutive integers
// (starting from provided start) on subsequent calls
func NextInts(current int) func() int {
	return func() int {
		ret := current
		current = current + 1
		return ret
	}
}

func main() {
	nextInt := NextInts(3)
	for i := 0; i < 10; i++ {
		fmt.Println(nextInt())
	}
}
