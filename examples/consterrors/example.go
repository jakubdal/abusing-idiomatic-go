package main

import (
	"errors"
	"fmt"
)

// START OMIT
var (
	ErrConst = errors.New("constant error")
)

func printError(err error) {
	switch err {
	case ErrConst:
		fmt.Println("print whatever")
	default:
		fmt.Println(err)
	}
}

func main() {
	var err error = ErrConst
	printError(err)
	printError(ErrConst)
	printError(errors.New("some error"))
	printError(errors.New("constant error"))
}

// END OMIT
