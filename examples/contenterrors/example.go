package main

import (
	"errors"
	"fmt"
)

// START OMIT
var (
	ContentError = "triggering content"
)

func printError(err error) {
	switch err.Error() {
	case ContentError:
		fmt.Println("We still have one message only:", err)
	default:
		fmt.Println("Default:", err)
	}
}

func main() {
	printError(errors.New(ContentError))
	printError(errors.New("different content"))
	printError(errors.New("triggering content"))
}

//END OMIT
